#!/usr/bin/env bash
mkdir -p /usr/local/tomcat
tar -xzf apache-tomcat-8.5.38.tar.gz -C /usr/local/tomcat --strip-components 1
rm apache-tomcat-8.5.38.tar.gz
echo $TOMCAT_PATH
cp *.xml $TOMCAT_PATH/conf
cp ./manager/context.xml $TOMCAT_PATH/webapps/manager/META-INF/