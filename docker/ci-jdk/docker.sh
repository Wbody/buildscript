#!/usr/bin/env bash
find . -name "*" -a -not -name 'docker.sh' | xargs dos2unix
imagename=$(basename `pwd`)
echo "image:" wbody/$imagename
dversion=8
read -p "enter version($dversion):" version
if test -z "$version"
then
    version=$dversion
fi
docker build -t "wbody/$imagename:$version" .
docker push "wbody/$imagename:$version"
