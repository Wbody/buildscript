#!/usr/bin/env bash
mkdir -p ${JAVA_HOME}
jdk_tar=$(ls jdk*.tar.gz)
tar -xzf ./${jdk_tar} -C ${JAVA_HOME} --strip-components 1
chmod -R 777  ${JAVA_HOME}
#rm ${jdk_tar}