#!/usr/bin/env bash
function echo_stage(){
  echo "the build's tag is ${CI_BUILD_TAG}"
  echo "the stage's name is ${CI_JOB_STAGE}"
}

echo_stage

function setTag(){
  export CI_BUILD_TAG=$1
  echo "CI_BUILD_TAG = ${CI_BUILD_TAG}"
}

function setStage(){
  export CI_JOB_STAGE=$1
  echo "CI_JOB_STAGE = ${CI_JOB_STAGE}"
}

