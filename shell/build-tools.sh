#!/usr/bin/env bash
export IMG_VERSION="0.0.1"
export BUILD_VERSION="0.0.0.1"
export IMAGE_IS_RELEASE=0
export IMAGE_IS_BETA=0
export IMAGE_IS_TEST=0

function docker_build() {
  ARG=$(echo ${CI_BUILD_TAG} | awk -F '-' '{print $1}')
  if [[ "$ARG" == "release" ]]; then
    IMAGE_IS_RELEASE=1
  elif [[ "$ARG" == "beta" ]]; then
    IMAGE_IS_BETA=1
  elif [[ "$ARG" == "test" ]]; then
    IMAGE_IS_TEST=1
  fi
  IMG_VERSION=$(echo ${CI_BUILD_TAG} | awk -F '.' '{print $1"."$2"."$3}')
  BUILD_VERSION=$(echo ${CI_BUILD_TAG} | awk -F '.' '{print $1"."$2"."$3"."$4}')
  echo "tag:${ARG}"
  echo "beta:${IMAGE_IS_RELEASE}"
  echo "release:${IMAGE_IS_BETA}"
  echo "test:${IMAGE_IS_TEST}"
  #foreach output
  do_list ./output
}

function doWar() {
  file2="$2"
  echo "war:${file2}"
  filename=$(basename ${file2} .war)
  echo "filename:${filename}"
  mkdir -p ./docker/$imagename/$imagename
  #      copy Dockerfile template into sub docker path
  cp $1/$file2 ./docker/$imagename/$file2 | cp ./buildscripts/docker/Dockerfile ./docker/$imagename/Dockerfile
  cd ./docker/$imagename
  unzip -oq $file2 -d $imagename
  rm $file2
  #      replace by template flag "VERSION","IMAGENAME"
  if [[ -f "$imagename/index.html" ]]; then
    sed -i "s/#VERSION/${BUILD_VERSION}/g" $imagename/index.html
  fi
  sed -i "s/#IMAGENAME/${imagename}/g" Dockerfile
}

function do_list() {
  ls -a
  for file1 in $(ls $1); do
    pwd
    echo "file1:${file1}"
    for file2 in $(ls $1/$file1); do
      echo "file2:${file2}"
      ext=${file2##*.}
      version=${file2##*-}
      imagename=$(basename ${file2} -$version)
      echo "ext:${ext}"
      echo "imagename:${imagename}"
      if [[ $ext == "war" ]]; then
        echo "doWar"
        doWar $1/$file1 $file2
      else
        echo $file2
      fi
      docker build -t "${GROUP_NAME}/$imagename:${IMG_VERSION}" .
      docker push "${GROUP_NAME}/$imagename:${IMG_VERSION}"
      docker rmi -f $(docker images -q "${GROUP_NAME}/$imagename:${IMG_VERSION}") || echo 'done'
      cd ../../
      pwd
    done
  done
}
